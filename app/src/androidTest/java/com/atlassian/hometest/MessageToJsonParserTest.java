package com.atlassian.hometest;

import com.atlassian.hometest.messageparser.MessageToJsonParser;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class MessageToJsonParserTest extends TestCase {

    public void testParseMessage_WhatToTest_WhatShouldBeResult() throws Exception {
        MessageToJsonParser messageToJsonParser = new MessageToJsonParser();

        String messageToParse = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
        JSONObject jsonObject = messageToJsonParser.parseMessage(messageToParse);

        // check not null
        assertNotNull(jsonObject);

        // check has links
        assertEquals(jsonObject.has("links"), true);

        JSONArray jsonArrayLinks = jsonObject.getJSONArray("links");

        // check we have only one link
        assertEquals(jsonArrayLinks.length(), 1);

        JSONObject jsonObjectLink = jsonArrayLinks.getJSONObject(0);
        assertEquals(jsonObjectLink.getString("url"), "https://twitter.com/jdorfman/status/430511497475670016");
        assertEquals(jsonObjectLink.getString("title"), "NBC Olympics | 2014 NBC Olympics in Sochi Russia"); // MOCK THIS!!!
    }
}