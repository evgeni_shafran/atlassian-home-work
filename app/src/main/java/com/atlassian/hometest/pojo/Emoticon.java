package com.atlassian.hometest.pojo;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class Emoticon {

    String emoticon;

    public String getEmoticon() {
        return emoticon;
    }

    public void setEmoticon(String emoticon) {
        this.emoticon = emoticon;
    }
}
