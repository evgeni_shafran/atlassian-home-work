package com.atlassian.hometest.pojo;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class Mention {

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
