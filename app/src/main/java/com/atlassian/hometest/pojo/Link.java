package com.atlassian.hometest.pojo;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class Link {

    private String url;
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
