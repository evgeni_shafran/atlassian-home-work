package com.atlassian.hometest.messageobjectdataprocessors;

/**
 * Created by Evgeni on 12/12/2015.
 */
public interface MessageObjectDataProcessor {

    /**
     * Fill the message object with data that was not in the initial message, such as title for links
     * @param messageObject
     */
    void processObject(Object messageObject);
}
