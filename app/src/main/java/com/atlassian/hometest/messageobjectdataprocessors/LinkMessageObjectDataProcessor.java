package com.atlassian.hometest.messageobjectdataprocessors;

import com.atlassian.hometest.net.ContentRetrieverFactory;
import com.atlassian.hometest.net.UrlContentRetriever;
import com.atlassian.hometest.pojo.Link;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class LinkMessageObjectDataProcessor implements MessageObjectDataProcessor {

    @Override
    public void processObject(Object messageObject) {
        if (!(messageObject instanceof Link)) {
            throw new IllegalArgumentException("LinkMessageObjectDataProcessor can only handle Link objects");
        }

        Link link = (Link) messageObject;

        try {
            String urlContent = new ContentRetrieverFactory().getUrlContentRetriver().getContentFromUrl(link.getUrl());

            Pattern pattern = Pattern.compile("\\<title>(.*)\\</title>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

            String title = "";

            Matcher matcher = pattern.matcher(urlContent);
            if (matcher.find()) {
                title = matcher.group(1);
            }

            link.setTitle(title);

        } catch (UrlContentRetriever.UrlLoadException e){
            // TODO: we need a product understanding on what to do when we are unable to load data from url
        }
    }
}
