package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.pojo.Emoticon;
import com.atlassian.hometest.pojo.Mention;

/**
 * Created by Evgeni on 12/11/2015.
 */
public class EmoticonMessageObjectJsonParser implements MessageObjectJsonParser {

    @Override
    public String objectToString(Object messageObject) {

        if (!(messageObject instanceof Emoticon)){
            throw new IllegalArgumentException("MentionMessageObjectJsonParser can only handle Mention objects");
        }

        Emoticon emoticon = (Emoticon) messageObject;

        return emoticon.getEmoticon();
    }
}
