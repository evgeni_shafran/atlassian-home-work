package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.pojo.Link;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Evgeni on 12/11/2015.
 */
public class LinkMessageObjectJsonParser implements MessageObjectJsonParser {

    public static final String JSON_OBJECT_NAME_URL = "url";
    public static final String JSON_OBJECT_NAME_TITLE = "title";

    @Override
    public String objectToString(Object messageObject) {

        if (!(messageObject instanceof Link)) {
            throw new IllegalArgumentException("LinkMessageObjectJsonParser can only handle Link objects");
        }

        Link link = (Link) messageObject;

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(JSON_OBJECT_NAME_URL, link.getUrl());
            jsonObject.put(JSON_OBJECT_NAME_TITLE, link.getTitle());
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }

        return jsonObject.toString();
    }
}
