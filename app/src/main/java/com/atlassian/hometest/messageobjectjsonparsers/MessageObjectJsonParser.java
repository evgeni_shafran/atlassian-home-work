package com.atlassian.hometest.messageobjectjsonparsers;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/11/2015.
 */

public interface MessageObjectJsonParser {
    /**
     *
     * @param messageObject
     * @return String representing the object<br/>
     * Can be a simple String for simple object<br/>
     * Can be JsonObject converted to string for more complicated object
     */
    String objectToString(Object messageObject);
}