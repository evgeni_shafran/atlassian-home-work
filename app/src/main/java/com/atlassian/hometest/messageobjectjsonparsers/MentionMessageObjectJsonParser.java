package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.pojo.Mention;

import org.json.JSONObject;

/**
 * Created by Evgeni on 12/11/2015.
 */
public class MentionMessageObjectJsonParser implements MessageObjectJsonParser {

    @Override
    public String objectToString(Object messageObject) {

        if (!(messageObject instanceof Mention)){
            throw new IllegalArgumentException("MentionMessageObjectJsonParser can only handle Mention objects");
        }

        Mention mention = (Mention) messageObject;

        return mention.getName();
    }
}
