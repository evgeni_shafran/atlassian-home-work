package com.atlassian.hometest.messageparser;

import com.atlassian.hometest.messageextractors.EmoticonMessageRegexExtractor;
import com.atlassian.hometest.messageextractors.LinkMessageRegexExtractor;
import com.atlassian.hometest.messageextractors.MentionMessageRegexExtractor;
import com.atlassian.hometest.messageobjectdataprocessors.LinkMessageObjectDataProcessor;
import com.atlassian.hometest.messageobjectjsonparsers.EmoticonMessageObjectJsonParser;
import com.atlassian.hometest.messageobjectjsonparsers.LinkMessageObjectJsonParser;
import com.atlassian.hometest.messageobjectjsonparsers.MentionMessageObjectJsonParser;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class ParseObjectHelperFactory {

    ArrayList <ParseObjectHelper> getParseObjectHelpers(){

        ArrayList <ParseObjectHelper> parseObjectHelpers = new ArrayList<>();
        parseObjectHelpers.add( new ParseObjectHelper("links", new LinkMessageRegexExtractor(), new LinkMessageObjectJsonParser(), new LinkMessageObjectDataProcessor()));
        parseObjectHelpers.add( new ParseObjectHelper("mentions", new MentionMessageRegexExtractor(), new MentionMessageObjectJsonParser(), null));
        parseObjectHelpers.add( new ParseObjectHelper("emoticons", new EmoticonMessageRegexExtractor(), new EmoticonMessageObjectJsonParser(), null));

        return parseObjectHelpers;
    }
}
