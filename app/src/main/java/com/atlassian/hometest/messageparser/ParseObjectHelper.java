package com.atlassian.hometest.messageparser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.atlassian.hometest.messageextractors.MessageObjectExtractor;
import com.atlassian.hometest.messageobjectdataprocessors.MessageObjectDataProcessor;
import com.atlassian.hometest.messageobjectjsonparsers.MessageObjectJsonParser;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class ParseObjectHelper {
    public ParseObjectHelper(
            @NonNull String name,
            @NonNull MessageObjectExtractor messageObjectExtractor,
            @NonNull MessageObjectJsonParser messageObjectJsonParser,
            @Nullable MessageObjectDataProcessor messageObjectDataProcessor) {
        this.messageObjectExtractor = messageObjectExtractor;
        this.name = name;
        this.messageObjectJsonParser = messageObjectJsonParser;
        this.messageObjectDataProcessor = messageObjectDataProcessor;
    }

    public String name;
    public MessageObjectExtractor messageObjectExtractor;
    public MessageObjectJsonParser messageObjectJsonParser;
    public MessageObjectDataProcessor messageObjectDataProcessor;
}
