package com.atlassian.hometest.messageparser;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.atlassian.hometest.messageextractors.LinkMessageRegexExtractor;
import com.atlassian.hometest.messageextractors.MentionMessageRegexExtractor;
import com.atlassian.hometest.messageextractors.MessageObjectExtractor;
import com.atlassian.hometest.messageobjectdataprocessors.LinkMessageObjectDataProcessor;
import com.atlassian.hometest.messageobjectdataprocessors.MessageObjectDataProcessor;
import com.atlassian.hometest.messageobjectjsonparsers.LinkMessageObjectJsonParser;
import com.atlassian.hometest.messageobjectjsonparsers.MentionMessageObjectJsonParser;
import com.atlassian.hometest.messageobjectjsonparsers.MessageObjectJsonParser;
import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class MessageToJsonParser {

    public MessageToJsonParser() {

    }

    public JSONObject parseMessage(String message){
        JSONObject parsedMessage = new JSONObject();

        ArrayList <ParseObjectHelper> parseObjectHelpers = new ParseObjectHelperFactory().getParseObjectHelpers();

        for (ParseObjectHelper parseObjectHelper: parseObjectHelpers) {

            ArrayList messageObjects;
            messageObjects = parseObjectHelper.messageObjectExtractor.extract(message);

            if (parseObjectHelper.messageObjectDataProcessor != null){
                for (Object messageObject : messageObjects) {
                    parseObjectHelper.messageObjectDataProcessor.processObject(messageObject);
                }
            }

            JSONArray jsonArray = new JSONArray();
            for (Object messageObject : messageObjects) {
                jsonArray.put(parseObjectHelper.messageObjectJsonParser.objectToString(messageObject));
            }

            try {
                parsedMessage.put(parseObjectHelper.name, jsonArray);
            } catch (JSONException e) {
                // this do not supposed to happen at all
                // TODO: Log something
            }
        }

        return parsedMessage;
    }
}
