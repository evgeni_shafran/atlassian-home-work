package com.atlassian.hometest.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class SimpleUrlContentRetriever implements UrlContentRetriever {
    @Override
    public String getContentFromUrl(String urlPath) {

        String content = "";
        BufferedReader in = null;
        StringBuffer stringBuffer = new StringBuffer();

        try {
            URL url = new URL(urlPath);

            in = new BufferedReader(
                    new InputStreamReader(
                            url.openStream()));

            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                stringBuffer.append(inputLine);
            }

            content = stringBuffer.toString();

        } catch (IOException e) {
            throw new UrlLoadException(e);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    throw new UrlLoadException(e);
                }
            }
        }

        return content;
    }
}
