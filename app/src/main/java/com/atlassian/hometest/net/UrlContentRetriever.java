package com.atlassian.hometest.net;

/**
 * Created by Evgeni on 12/12/2015.
 */
public interface UrlContentRetriever {

    public static class UrlLoadException extends RuntimeException{
        public UrlLoadException(Throwable throwable) {
            super(throwable);
        }

        public UrlLoadException(String detailMessage, Throwable throwable) {
            super(detailMessage, throwable);
        }

        public UrlLoadException(String detailMessage) {
            super(detailMessage);
        }

        public UrlLoadException() {
            super();
        }
    }

    /**
     *
     * @param url
     * @return
     * @throws UrlLoadException if any error accured while trying to download/ parse content
     */
    String getContentFromUrl(String url) throws UrlLoadException;
}
