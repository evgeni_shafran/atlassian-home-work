package com.atlassian.hometest.net;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class ContentRetrieverFactory {

    private static UrlContentRetriever urlContentRetriver;

    public UrlContentRetriever getUrlContentRetriver (){
        if (urlContentRetriver == null){
            urlContentRetriver = new SimpleUrlContentRetriever();
        }

        return urlContentRetriver;
    }
}
