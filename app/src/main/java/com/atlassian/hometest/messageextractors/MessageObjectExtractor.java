package com.atlassian.hometest.messageextractors;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/9/2015.
 */
public interface MessageObjectExtractor <E>{
    ArrayList<E> extract(String message);
}
