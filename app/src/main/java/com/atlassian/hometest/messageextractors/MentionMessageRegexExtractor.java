package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;

import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class MentionMessageRegexExtractor extends MessageRegexExtractor<Mention> {

    @Override
    protected void addItem(ArrayList<Mention> list, Matcher matcher) {
        Mention mention = new Mention();
        mention.setName(matcher.group().substring(1));
        list.add(mention);
    }

    @Override
    protected String getRegexPattern() {
        return "@\\S+";
    }
}
