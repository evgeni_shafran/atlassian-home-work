package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.pojo.Emoticon;
import com.atlassian.hometest.pojo.Mention;

import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class EmoticonMessageRegexExtractor extends MessageRegexExtractor<Emoticon> {

    @Override
    protected void addItem(ArrayList<Emoticon> list, Matcher matcher) {
        Emoticon emoticon = new Emoticon();
        String emoticonString = matcher.group();
        emoticonString = emoticonString.substring(1, emoticonString.length() - 1);

        // We should probably do this in the regex but due to time limit and me hating regex here we go:
        if (emoticonString.length() <= 15) {
            emoticon.setEmoticon(emoticonString);
            list.add(emoticon);
        }
    }

    @Override
    protected String getRegexPattern() {
        return "\\(\\S+\\)";
    }
}
