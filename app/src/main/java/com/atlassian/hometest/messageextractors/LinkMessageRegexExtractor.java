package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.pojo.Link;

import java.util.ArrayList;
import java.util.regex.Matcher;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class LinkMessageRegexExtractor extends MessageRegexExtractor<Link> {

    @Override
    protected void addItem(ArrayList<Link> list, Matcher matcher) {
        Link link = new Link();
        link.setUrl(matcher.group());
        list.add(link);
    }

    @Override
    protected String getRegexPattern() {
        return "\\bhttps?:\\/\\/\\S+"; // TODO: should probably be more precise
    }
}
