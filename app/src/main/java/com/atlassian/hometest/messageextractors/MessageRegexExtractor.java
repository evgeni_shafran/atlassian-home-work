package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.pojo.Link;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Evgeni on 12/9/2015.
 */
public abstract class MessageRegexExtractor<E> implements MessageObjectExtractor <E>{

    @Override
    public ArrayList<E> extract(String message) {

        ArrayList<E> list = new ArrayList<>();

        Pattern pattern = Pattern.compile(getRegexPattern());
        Matcher matcher = pattern.matcher(message);

        while(matcher.find()) {
            addItem(list, matcher);
        }

        return list;
    }

    protected abstract void addItem(ArrayList<E> list, Matcher matcher);

    protected abstract String getRegexPattern();
}
