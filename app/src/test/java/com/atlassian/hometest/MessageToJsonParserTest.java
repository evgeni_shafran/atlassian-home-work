package com.atlassian.hometest;

import com.atlassian.hometest.messageparser.MessageToJsonParser;

import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class MessageToJsonParserTest extends TestCase {

    public void testParseMessage_ParseMessage_VerifyData2Mentions1Emoticon1Link() throws Exception {
        MessageToJsonParser messageToJsonParser = new MessageToJsonParser();

        // WE NEED TO MOCK EXTRACTORS IN ORDER THIS TO BE A UNIT TEST AND NOT COMPONENT TEST

        // TODO: we should mock the actual url content retrieval
        JSONObject jsonObject = messageToJsonParser.parseMessage(MessagesTestHelper.MESSAGE_2MENTIONS_1EMOTIOCON_1LINK);

        // check not null
        assertNotNull(jsonObject);

        // check has links
        assertEquals(jsonObject.has("links"), true);

        JSONArray jsonArrayLinks = jsonObject.getJSONArray("links");

        // check we have only one link
        assertEquals(jsonArrayLinks.length(), 1);

        JSONObject jsonObjectLink = new JSONObject(jsonArrayLinks.getString(0));
        assertEquals(MessagesTestHelper.URL1, jsonObjectLink.getString("url"));
        // this url is retrieved different answers for title... we must mockthis :) or it going to drive me crazy
        //  assertEquals(MessagesTestHelper.URL1_TITLE, jsonObjectLink.optString("title")); // MOCK THIS!!!

        // check has mentions
        assertEquals(true, jsonObject.has("mentions"));
        JSONArray jsonArrayMentions = jsonObject.getJSONArray("mentions");

        // check do we have 2 mentions
        assertEquals(jsonArrayMentions.length(), 2);

        assertEquals(MessagesTestHelper.MENTION1, jsonArrayMentions.getString(0));
        assertEquals(MessagesTestHelper.MENTION2, jsonArrayMentions.getString(1));

        // check emoticons
        assertEquals(true, jsonObject.has("emoticons"));

        JSONArray jsonArrayEmotions = jsonObject.getJSONArray("emoticons");

        // check do we have 1 emotion
        assertEquals(jsonArrayEmotions.length(), 1);

        assertEquals(MessagesTestHelper.MENTION1, jsonArrayMentions.getString(0));
    }
}