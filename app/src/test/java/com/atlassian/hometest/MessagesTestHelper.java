package com.atlassian.hometest;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class MessagesTestHelper {

    public final static String URL1 = "https://twitter.com/jdorfman/status/430511497475670016";
    public final static String URL2 = "http://yapplications.com/wp/";
    public final static String URL3 = "https://bbc.com";

    public static final String URL1_TITLE = "Justin Dorfman on Twitter: &quot;nice @littlebigdetail from @HipChat (shows hex colors when pasted in chat). http://t.co/7cI6Gjy5pq&quot;";
    public static final String URL2_TITLE = "yapplications | Yavji Application";

    public final static String MENTION1 = "bob";
    public final static String MENTION2 = "john";

    public static final String EMOTICON1 = "success";

    public final static String MESSAGE_2MENTIONS_1EMOTIOCON_1LINK = "@" + MENTION1 + " @" + MENTION2 + " (" + EMOTICON1 + ") such a cool feature; " + URL1;

    public final static String MESSAGE_0MENTIONS_1EMOTIOCON_1LINK = " (" + EMOTICON1 + ") such a cool feature; " + URL1;

    public final static String MESSAGE_ONLY_ONE_MENTION = "@" + MENTION1;

    public final static String MESSAGE_2MENTIONS_1EMOTIOCON_0LINK = "@bob @john (" + EMOTICON1 + ") such a cool feature;";

    public final static String MESSAGE_ONLY_1LINK = URL1;

    public final static String MESSAGE_3MENTIONS_1EMOTIOCON_3LINK = "@bob @john (" + EMOTICON1 + ") such a cool feature; " + URL1 + " test " + URL2 + " @sam " + URL3;

    public static final String URL1_CONTENT = "";
}
