package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.junit.Test;

/**
 * Created by Evgeni on 12/11/2015.
 */
public class MentionMessageObjectJsonParserTest extends TestCase {

    public void testObjectToString_TryToParseLink_ThrowException() throws Exception {
        Link link = new Link();

        MentionMessageObjectJsonParser mentionMessageObjectJsonParser = new MentionMessageObjectJsonParser();

        try {
            mentionMessageObjectJsonParser.objectToString(link);
            Assert.fail("Was supposed to throw exception");
        } catch (IllegalArgumentException e){

        }
    }

    public void testObjectToString_TryToParseMention_ValidateResult() throws Exception {
        Mention mention = new Mention();

        MentionMessageObjectJsonParser mentionMessageObjectJsonParser = new MentionMessageObjectJsonParser();
        mention.setName(MessagesTestHelper.MENTION1);

        assertEquals(MessagesTestHelper.MENTION1, mentionMessageObjectJsonParser.objectToString(mention));
    }
}