package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Emoticon;
import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;

import junit.framework.Assert;
import junit.framework.TestCase;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class EmoticonMessageObjectJsonParserTest extends TestCase {
    public void testObjectToString_TryToParseLink_ThrowException() throws Exception {
        Link link = new Link();

        EmoticonMessageObjectJsonParser emoticonMessageObjectJsonParser = new EmoticonMessageObjectJsonParser();

        try {
            emoticonMessageObjectJsonParser.objectToString(link);
            Assert.fail("Was supposed to throw exception");
        } catch (IllegalArgumentException e){

        }
    }

    public void testObjectToString_TryToParseEmoticon_ValidateResult() throws Exception {
        Emoticon emoticon = new Emoticon();

        EmoticonMessageObjectJsonParser emoticonMessageObjectJsonParser = new EmoticonMessageObjectJsonParser();
        emoticon.setEmoticon(MessagesTestHelper.EMOTICON1);

        assertEquals(MessagesTestHelper.EMOTICON1, emoticonMessageObjectJsonParser.objectToString(emoticon));
    }
}