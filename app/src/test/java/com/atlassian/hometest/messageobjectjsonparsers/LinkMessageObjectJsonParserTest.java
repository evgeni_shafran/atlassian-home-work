package com.atlassian.hometest.messageobjectjsonparsers;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.json.JSONObject;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class LinkMessageObjectJsonParserTest extends TestCase {

    public void testObjectToString() throws Exception {

    }

    public void testObjectToString_TryToParseMention_ThrowException() throws Exception {
        Mention mention = new Mention();

        LinkMessageObjectJsonParser linkMessageObjectJsonParser = new LinkMessageObjectJsonParser();

        try {
            linkMessageObjectJsonParser.objectToString(mention);
            Assert.fail("Was supposed to throw exception");
        } catch (IllegalArgumentException e){

        }
    }

    public void testObjectToString_TryToParseLink_ValidateResult() throws Exception {
        Link link = new Link();

        LinkMessageObjectJsonParser linkMessageObjectJsonParser = new LinkMessageObjectJsonParser();

        link.setUrl(MessagesTestHelper.URL1);
        link.setTitle(MessagesTestHelper.URL1_TITLE);
        JSONObject jsonObject = new JSONObject(linkMessageObjectJsonParser.objectToString(link));

        assertEquals(MessagesTestHelper.URL1, jsonObject.get(LinkMessageObjectJsonParser.JSON_OBJECT_NAME_URL));
        assertEquals(MessagesTestHelper.URL1_TITLE, jsonObject.get(LinkMessageObjectJsonParser.JSON_OBJECT_NAME_TITLE));
    }
}