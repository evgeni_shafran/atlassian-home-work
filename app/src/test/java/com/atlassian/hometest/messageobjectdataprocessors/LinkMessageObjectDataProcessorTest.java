package com.atlassian.hometest.messageobjectdataprocessors;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.net.ContentRetrieverFactory;
import com.atlassian.hometest.net.UrlContentRetriever;
import com.atlassian.hometest.pojo.Link;
import com.atlassian.hometest.pojo.Mention;
import static org.mockito.Mockito.*;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.mockito.Mockito;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class LinkMessageObjectDataProcessorTest extends TestCase {

    public void testObjectToString_TryToParseMention_ThrowException() throws Exception {
        Mention mention = new Mention();

        LinkMessageObjectDataProcessor linkMessageObjectDataProcessor = new LinkMessageObjectDataProcessor();

        try {
            linkMessageObjectDataProcessor.processObject(mention);
            Assert.fail("Was supposed to throw exception");
        } catch (IllegalArgumentException e){

        }
    }

    public void testObjectToString_TryToProcessLink_VerifyData() throws Exception {

        Link link = new Link();
        link.setUrl(MessagesTestHelper.URL2);

        // TODO: we should mock the actual url content retrieval
        LinkMessageObjectDataProcessor linkMessageObjectDataProcessor = new LinkMessageObjectDataProcessor();
        linkMessageObjectDataProcessor.processObject(link);

        assertEquals(MessagesTestHelper.URL2_TITLE, link.getTitle());
    }
}