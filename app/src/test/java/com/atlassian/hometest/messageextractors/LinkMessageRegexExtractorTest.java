package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Link;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/9/2015.
 */
public class LinkMessageRegexExtractorTest extends TestCase {

    public void testExtract_MessageWithLinks_VerifyOneLink() throws Exception {
        LinkMessageRegexExtractor messageLinkExtractor = new LinkMessageRegexExtractor();

        ArrayList<Link> links = messageLinkExtractor.extract(MessagesTestHelper.MESSAGE_2MENTIONS_1EMOTIOCON_1LINK);

        assertNotNull(links);

        assertEquals(1, links.size());

        Link link = links.get(0);

        assertEquals(MessagesTestHelper.URL1, link.getUrl());
    }

    public void testExtract_MessageWithout_VerifyNoLink() throws Exception {
        LinkMessageRegexExtractor messageLinkExtractor = new LinkMessageRegexExtractor();

        ArrayList<Link> links = messageLinkExtractor.extract(MessagesTestHelper.MESSAGE_2MENTIONS_1EMOTIOCON_0LINK);

        assertNotNull(links);

        assertEquals(0, links.size());
    }

    public void testExtract_MessageWithOnlyLink_Verify3Links() throws Exception {
        LinkMessageRegexExtractor messageLinkExtractor = new LinkMessageRegexExtractor();

        ArrayList<Link> links = messageLinkExtractor.extract(MessagesTestHelper.MESSAGE_ONLY_1LINK);

        assertNotNull(links);

        assertEquals(1, links.size());

        Link link = links.get(0);

        assertEquals(MessagesTestHelper.URL1, link.getUrl());
    }

    public void testExtract_MessageWithLinks_Verify3Links() throws Exception {
        LinkMessageRegexExtractor messageLinkExtractor = new LinkMessageRegexExtractor();

        ArrayList<Link> links = messageLinkExtractor.extract(MessagesTestHelper.MESSAGE_3MENTIONS_1EMOTIOCON_3LINK);

        assertNotNull(links);

        assertEquals(3, links.size());

        Link link = links.get(0);

        assertEquals(MessagesTestHelper.URL1, link.getUrl());

        link = links.get(1);

        assertEquals(MessagesTestHelper.URL2, link.getUrl());

        link = links.get(2);

        assertEquals(MessagesTestHelper.URL3, link.getUrl());
    }
}