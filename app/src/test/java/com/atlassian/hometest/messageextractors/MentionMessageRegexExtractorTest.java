package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Mention;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/10/2015.
 */
public class MentionMessageRegexExtractorTest extends TestCase {

    public void testExtract_MessageWithMentions_Verify2Mentions() throws Exception {
        MentionMessageRegexExtractor mentionMessageRegexExtractor = new MentionMessageRegexExtractor();

        ArrayList<Mention> mentions = mentionMessageRegexExtractor.extract(MessagesTestHelper.MESSAGE_2MENTIONS_1EMOTIOCON_1LINK);

        assertNotNull(mentions);

        assertEquals(2, mentions.size());

        Mention mention = mentions.get(0);
        assertEquals(MessagesTestHelper.MENTION1, mention.getName());

        mention = mentions.get(1);
        assertEquals(MessagesTestHelper.MENTION2, mention.getName());
    }

    public void testExtract_MessageWithNoMentions_Verify0Mentions() throws Exception {
        MentionMessageRegexExtractor mentionMessageRegexExtractor = new MentionMessageRegexExtractor();

        ArrayList<Mention> mentions = mentionMessageRegexExtractor.extract(MessagesTestHelper.MESSAGE_0MENTIONS_1EMOTIOCON_1LINK);

        assertNotNull(mentions);

        assertEquals(0, mentions.size());
    }

    public void testExtract_MessageWithOnlyMention_Verify1Mentions() throws Exception {
        MentionMessageRegexExtractor mentionMessageRegexExtractor = new MentionMessageRegexExtractor();

        ArrayList<Mention> mentions = mentionMessageRegexExtractor.extract(MessagesTestHelper.MESSAGE_ONLY_ONE_MENTION);

        assertNotNull(mentions);

        assertEquals(1, mentions.size());

        Mention mention = mentions.get(0);
        assertEquals(MessagesTestHelper.MENTION1, mention.getName());
    }
}