package com.atlassian.hometest.messageextractors;

import com.atlassian.hometest.MessagesTestHelper;
import com.atlassian.hometest.pojo.Emoticon;
import com.atlassian.hometest.pojo.Mention;

import junit.framework.TestCase;

import java.util.ArrayList;

/**
 * Created by Evgeni on 12/12/2015.
 */
public class EmoticonMessageRegexExtractorTest extends TestCase {
    public void testExtract_MessageWithEmotion_Verify1Emotion() throws Exception {
        EmoticonMessageRegexExtractor emoticonMessageRegexExtractor = new EmoticonMessageRegexExtractor();

        ArrayList<Emoticon> emoticons = emoticonMessageRegexExtractor.extract(MessagesTestHelper.MESSAGE_2MENTIONS_1EMOTIOCON_1LINK);

        assertNotNull(emoticons);

        assertEquals(1, emoticons.size());

        Emoticon emoticon = emoticons.get(0);
        assertEquals(MessagesTestHelper.EMOTICON1, emoticon.getEmoticon());
    }

}